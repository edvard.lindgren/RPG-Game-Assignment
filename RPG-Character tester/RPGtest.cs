using System;
using Xunit;

namespace Rpg_game
{
    public class RPGtestgitla
    {

        public Weapon Axe = new Weapon()
        {
            NameOfItem = "Common axe",
            LevelToEquip = 2,
            EqSlot = Items.Slot.Weapon,
            TypeOfWeapon = Weapon.WeaponType.Axes,
            Damage = 2,
            WeaponAs = 2.2
            

        };
        /// <summary>
        /// Test if Weaponattributes function calculate the correct DPS
        /// I do miss every single test beside this one as I am missing the functionality to test
        /// </summary>
        [Fact]
        public void TestDPSNoWeapon()
        {
            //Arrange

            double expected = 2 * 2.2;

            //Act
            
            double actual = Axe.WeaponAttributes(2, 2.2);

            //Assert
            Assert.Equal(expected, actual);
        }

 
    }
}
 