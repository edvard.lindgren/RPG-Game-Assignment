﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    /// <summary>
    /// Abstract class that set paramters which the Armor and Weapon classes can inherit from
    /// </summary>
    public abstract class Items
    {
        public string NameOfItem { get; set; }
        public int LevelToEquip { get; set; }
        public PrimaryAttributes BaseArmorPrimaryAttributes { get; set; }
        public Slot EqSlot { get; set; }
        public enum Slot
        {
            Head,
            Body,
            Legs,
            Weapon
        }

    }
}
