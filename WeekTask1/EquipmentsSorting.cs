﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    class EquipmentSorting
    {
        public string Head { get; set; }
        public string Chest { get; set; }
        public string Legs { get; set; }
        public string Weapon { get; set; }
        
        
        public enum Slot
        {
            Head,
            Chest,
            Legs,
            Weapon
        }

        public EquipmentSorting()
        {
             
        }

        public EquipmentSorting(Slot slot)
        {
            Head = Slot.Head.ToString();
            Chest = Slot.Chest.ToString();
            Legs = Slot.Legs.ToString();
            Weapon = Slot.Weapon.ToString();
        }

        public Dictionary<EquipmentSorting.Slot, EquipmentSorting> Equipment = new Dictionary<EquipmentSorting.Slot, EquipmentSorting>
        {
            {Slot.Head, null },
            {Slot.Chest, null },
            {Slot.Legs, null },
            {Slot.Weapon, null },
        };


        public void Equip(EquipmentSorting slot)
        {
            Console.WriteLine($"My slot is in the {Slot.Chest}");
        }
    }
}
