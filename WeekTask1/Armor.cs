﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    public class Armor : Items
    {

        public ArmorType TypeOfArmor { get; set; }
        public int ItemStats { get; set; }
        public int StatsOfArmor { get; set; }

        //Enumerate for storing different type of Armor
        public enum ArmorType
        {
            Cloths,
            Leathers,
            Mails,
            Plates,
        }

        //Default Armor
        public Armor()
        {

        }

        /// <summary>
        /// Armor creation, let us assign armor to the different classes and characters
        /// </summary>
        /// <param name="nameOfItem">Whats the name of the item</param>
        /// <param name="levelToEquip">What level requirment does the armor have</param>
        /// <param name="armorType">Set what type of armor is used or could be worn</param>
        public Armor(String nameOfItem, int levelToEquip, ArmorType typeOfArmor, Slot slot)
        {
            //var PrimaryArmor = new PrimaryAttributes()
            //{
            //    Strength = 2,
            //    Dexterity = 2,
            //    Intelligence = 2,
            //    Vitality = 2
            //};
            //BaseArmorPrimaryAttributes = PrimaryArmor;

            NameOfItem = nameOfItem;
            LevelToEquip = levelToEquip;
            TypeOfArmor = typeOfArmor;
            if (slot == Slot.Head )
            {
                EqSlot = Slot.Head;
            }
            else if (slot == Slot.Body)
            {
                EqSlot = Slot.Body;
            }
            else if (slot == Slot.Legs)
            {
                EqSlot = Slot.Legs;
            }
            //StatsOfArmor = SetGearAttributes(ItemStats);
            Console.WriteLine($"You equiped an {NameOfItem}, level req is it has a dps of and as of the type is {TypeOfArmor} and it contains {EqSlot}");
        }

        public static PrimaryAttributes SetGearAttributes(ArmorType typeOfArmor)
        {
                PrimaryAttributes ItemStats = new PrimaryAttributes();
            {
                if (typeOfArmor == Armor.ArmorType.Cloths)
                {
                    ItemStats = new PrimaryAttributes(0, 0, 0, 2);
                }
                else if (typeOfArmor == Armor.ArmorType.Leathers)
                {
                    ItemStats = new PrimaryAttributes(0, 0, 2, 0);
                }
                else if (typeOfArmor == Armor.ArmorType.Mails)
                {
                    ItemStats = new PrimaryAttributes(0, 2, 0, 0);
                }
                else if (typeOfArmor == Armor.ArmorType.Plates)
                {
                    ItemStats = new PrimaryAttributes(2, 0, 0, 0);
                }
                return ItemStats;
            }
        }
    }  
}
