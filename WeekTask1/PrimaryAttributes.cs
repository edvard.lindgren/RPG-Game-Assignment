﻿using System;

namespace Rpg_game
{
    //Class to crate primary attributes
    public class PrimaryAttributes
    {

        public int Vitality { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// Set values for primary attributes to 0
        /// </summary>
        public PrimaryAttributes()
        {
            Vitality = 0;
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;

        }
        /// <summary>
        /// Setting values for strength, vitality, dexterity and intelligence
        /// </summary>
        /// <param name="strength">Set the strength value for a char</param>
        /// <param name="vitality">Set the Vitality value for a char</param>
        /// <param name="dexterity">Set the Dexterity value for a char</param>
        /// <param name="intelligence">Set the Intelligence value for a char</param>
        public PrimaryAttributes(int strength, int vitality, int dexterity, int intelligence)
        {
            Strength = strength;
            Vitality = vitality;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

    }
}