﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    public class Weapon : Items
    {
        public int Damage { get; set; }
        public double WeaponAs { get; set; }
        public double DamagePerSecond { get; set; }
        public WeaponType TypeOfWeapon { get; set; }

        //Storing Weapons in an Enumerate
        public enum WeaponType
        {

            Axes,
            Bows,
            Daggers,
            Hammers,
            Staffs,
            Swords,
            Wands
        }
        //Default weapon
        public Weapon()
        {

        }
        /// <summary>
        /// Create a weapon with a name, level needed to equip, What weapon it is and weapon dps
        /// </summary>
        /// <param name="nameOfItem">Set the name of the weapon to a variable</param>
        /// <param name="levelToEquip">Set what level is required to equip the item to a variable</param>
        /// <param name="typeOfWeapon">Takes the weapons from the enumerate and assign them to a string to a variable</param>
        public Weapon(String nameOfItem, int levelToEquip, WeaponType typeOfWeapon, int Damage, double WeaponAs)
        {
            NameOfItem = nameOfItem.ToString();
            LevelToEquip = levelToEquip;
            EqSlot = Slot.Weapon;
            WeaponAttributes(Damage, WeaponAs);
            //DamagePerSecond = Damage * WeaponAs;
            TypeOfWeapon = typeOfWeapon;
            Console.WriteLine($"You equiped an {NameOfItem}, level req is {LevelToEquip} it" +
                $" has a dps of {DamagePerSecond} and as of the type is {TypeOfWeapon}. " +
                $"It was equiped in the {EqSlot} slot ");
        }

        public double WeaponAttributes(int damage, double weaponAs)
        {
            Damage = damage;
            WeaponAs = weaponAs;
            return DamagePerSecond = Damage * WeaponAs;
        }

        //public void CalcDps()
        //{
        //    double WepDps = ;
        //    Console.WriteLine($"The dps of the weapon is {WepDps}");
        //}
    }
}
