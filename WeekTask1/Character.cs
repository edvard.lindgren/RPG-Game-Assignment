﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    /// <summary>
    /// Abstract class which the different Hero classes can inherit from
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public string WhatClass { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }

        //public PrimaryAttributes TotalPrimaryAttributes { get; set; }

        // public SecondaryAttributes SecondaryAttributes { get; set; }

        public List<Weapon.WeaponType> EquipAbleWeapons;
        public List<Armor.ArmorType> EquipAbleArmor;

    }
}
