﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rpg_game
{
    class Ranger : Character


    {

        /// <summary>
        /// Creating a warrior starting at level 1 with base stats. Takes inn WhatClass and Level from the abstract class Character.
        /// </summary>
        /// <param name="name">Takes in the name parameter on its creation</param>
        public Ranger(string name)
        {

            //PrimaryAttributes are set to the level 1 Ranger stats
            Name = name;
            Level = 1;
            WhatClass = "Ranger";
            PrimaryAttributes baseAttri = new PrimaryAttributes(1, 8, 7, 1);
            BasePrimaryAttributes = baseAttri;
            EquipAbleWeapons = new()
            {
                Weapon.WeaponType.Bows
            };
            EquipAbleArmor = new()
            {
                Armor.ArmorType.Mails,
                Armor.ArmorType.Leathers
            };

            Console.WriteLine($"Im a level {Level} Ranger named {Name} My base stats are Vitality: {BasePrimaryAttributes.Vitality}, STR: {BasePrimaryAttributes.Strength}" +
                $" DEX: {BasePrimaryAttributes.Dexterity} and INT: {BasePrimaryAttributes.Intelligence}");

        }


        /// <summary>
        /// Increases the level of a Ranger and increases its primary stats
        /// </summary>
        /// <param name="NewLevel"></param>
        public void LevelUp(int NewLevel)
        {
            if (NewLevel <= 0)
            {
                Console.WriteLine($"You have to gain atleast 1 level");
            }
            else
            {
                Level = NewLevel;
                BasePrimaryAttributes = new PrimaryAttributes(5 + 3 * Level, 10 + 5 * Level, 2 + 2 * Level, 1 + 1 * Level);
                Console.WriteLine($"Your new stats are STR: {BasePrimaryAttributes.Strength} VIT: {BasePrimaryAttributes.Vitality}" +
                    $" DEX: {BasePrimaryAttributes.Dexterity} INT: {BasePrimaryAttributes.Intelligence}");
            }
        }
        /// <summary>
        /// Creating a dictionary called Gear to store equipment
        /// </summary>
        public Dictionary<Items.Slot, Items> Gear = new Dictionary<Items.Slot, Items>
        {
            {Items.Slot.Head, null },
            {Items.Slot.Body, null },
            {Items.Slot.Legs, null },
            {Items.Slot.Weapon, null },
        };


        /// <summary>
        /// Equip gear in the associated slot in the Gear Dictionary
        /// </summary>
        /// <param name="item"> Takes inn an item to compare to the equipment slot</param>
        /// <returns></returns>
        public string EquipGear(Items item)
        {
            if (item.EqSlot == Items.Slot.Weapon)
            {
                Gear[Items.Slot.Weapon] = item;
                return ($"Your weapon {item} is equiped");
            }
            else if (item.EqSlot == Items.Slot.Head)
            {
                Gear[Items.Slot.Head] = item;
                return ($"Your head {item} is equiped");
            }
            else if (item.EqSlot == Items.Slot.Body)
            {
                Gear[Items.Slot.Body] = item;
                return ($"Your Body {item} is equiped");
            }
            else if (item.EqSlot == Items.Slot.Legs)
            {
                Gear[Items.Slot.Legs] = item;
                return ($"Your Legs {item} is equiped");
            }
            else
            {
                return ($"Cant equip");
            }

        }

        public void UpdateAttributes()
        {
        }
        /// <summary>
        /// A Ranger need to shoot a bow
        /// </summary>
        public void Shoot()
        {
            Console.WriteLine($"Wiiiiiish my arrow hit the target because of my awesome {Weapon.WeaponType.Bows}");
        }
    }
}
